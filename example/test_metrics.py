#!/usr/bin/env python2

from metrics import UncertaintyMetric
from ase.io import read 
import numpy as np 
images = 'trainingset.traj'
calc = 'amp.amp'
metric = UncertaintyMetric(images=images, calculator=calc, normalized=True) 

test = read("test.traj", index='0:2')
fps1 = metric.get_trainingset_feature_vectors(elementwise=False)
fps2 = metric.get_target_feature_vectors(image=test, elementwise=False, 
	save_pickle=True)

confidence = metric.get_confidence(fps1, fps2[1], method='min', 
	                               pca=True, n_components=20)
print(confidence, )
