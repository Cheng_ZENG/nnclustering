from nnclustering import NNClustering
from ase.io import read 
images = 'trainingset.traj'
test_images = 'test.traj'
atoms = read(test_images, index=':')
nnclustering = NNClustering(images, calculator='amp.amp', normalized=False)

# print type(nnclustering)
a, b = nnclustering.clustering(n_clusters=2, pca=True, n_components=40)
# a, b = nnclustering.clustering(n_clusters=2,)
c, d = nnclustering.subsampling(cutoff_sig=0.70, rate=0.72,)

affinity = nnclustering.affinity(image=atoms[100], method='min')

print affinity