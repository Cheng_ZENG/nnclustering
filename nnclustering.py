"""
The intention is to extract useful information from the training set.
Training over a large domain of the fingerprint space is not recommended
because in this case it can be extremely difficult to find a converged
neural network (NN) model. The idea is inspired from the approach of
'divide and conquer' in computer science. We would like to know how the
training images distribute in the space of interest, with which we could
do more detailed uncertainty analysis, figure out how to effectively
chose the training set, and why the training fails sometimes. We use
clustering algorithms existent in `sklearn`, till now `KMeans` is used
in this module. Note that we use the feature vectors for clustering, we
can also use last layer vectors.
"""
from __future__ import print_function, division
from amp.utilities import hash_images
from metrics import UncertaintyMetric
from NNSubsampling import subsampling
from collections import OrderedDict

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn import metrics, preprocessing
from operator import itemgetter
import numpy as np
import pickle


class NNClustering(object):
    """
    Parameters
    ------------
    images : list or str
    List of ASE atoms objects with positions, symbols, energies, and
    forces in ASE format. This is the training set of data. This can
    also be the path to an ASE trajectory (.traj) or database (.db)
    file. Energies can be obtained from any reference, e.g. DFT
    calculations.

    calculator: Amp calculator
    Amp calculator, the machine learning model trained from the images

    normalized: boolean
    Prescale the vectors as constructed the training set

    force: boolean
    Training force or not
    """

    def __init__(self, images, calculator, normalized=False,
                 force=False):
        metric = UncertaintyMetric(images=images, calculator=calculator,
                                   normalized=normalized, force=force)
        afps = metric.get_trainingset_feature_vectors(save_pickle=False)
        self.afps = afps
        self.images = hash_images(images, ordered=True)
        self.normalized = normalized
        self.clustered = False
        self.metric = metric

    def clustering(self, pca=False, n_components=None, scaler='maxabs',
                   n_clusters=5, target_score=-1.0, save_pickle=False):
        """Clustering the atomic fingerprints (afps) of the training
        images.

        Parameters
        ------------
        pca: bool
        If pca is implemented before clustering.

        n_components: int
        Number of principal components in the PCA transformation. Only used
        if pca is **True**

        scaler: str
        The algorithm used to scale the raw data. Only used if normalized
        is **True**

        n_clusters: int
        Number of clusters

        target_score: float
        Clustering score which evaluates the performance. Higher score means
        dense and well seperated clusters. Here we use the silhouette score
        (see https://scikit-learn.org/stable/modules/clustering.html)

        **Note** Based on our expereiences, PCA transformation and Scaling
        should not be used together for high-dimension data.
        """
        afps = self.afps
        self.pca = pca
        images = self.images.values()

        if self.normalized:
            assert scaler in ['maxabs', 'minmax', 'standard']
            if scaler == 'maxabs':
                scaler = preprocessing.MaxAbsScaler()
                afps_scaled = scaler.fit_transform(afps)
            elif scaler == 'minmax':
                scaler = preprocessing.MinMaxScaler()
                afps_scaled = scaler.fit_transform(afps)
            elif scaler == 'standard':
                scaler = preprocessing.StandardScaler()
                afps_scaled = scaler.fit_transform(afps)
            self.scaler = scaler
        else:
            afps_scaled = afps
        if pca:
            assert isinstance(n_components, int)
            pca = PCA(n_components=n_components)
            afps_scaled = pca.fit_transform(afps_scaled)
            self.pca_tranform = pca
        self.afps = afps_scaled
        kmeans_model = KMeans(n_clusters=n_clusters).fit(afps_scaled)
        labels = kmeans_model.labels_
        score = metrics.silhouette_score(afps_scaled, labels,
                                         metric='euclidean')
        if score <= target_score:
            raise ClusteringFailureError('Clustering does not work!')
        clustered_images = OrderedDict()
        clustered_afps = OrderedDict()
        for i in range(n_clusters):
            indices = np.where(labels == i)[0]
            afps_per_cluster = itemgetter(*indices)(afps_scaled)
            images_per_cluster = itemgetter(*indices)(images)
            clustered_images[i] = images_per_cluster
            clustered_afps[i] = afps_per_cluster
        self.clustered = True
        self.clustered_images = clustered_images
        self.clustered_afps = clustered_afps
        if save_pickle:
            with open('clustering.pkl', 'wb') as pf:
                pickle.dump(clustered_images, pf)
                pickle.dump(clustered_afps, pf)
        return clustered_images, clustered_afps

    def subsampling(self, cutoff_sig=0.60, rate=0.80, save_pickle=False):
        """Subsampling each cluster by using a nearest-neighbor based
        algorithm. The subsampling method used here lives in Ray's github:
        https://github.com/ray38/NNSubsampling

        **Note**: for high-dimentional feature vectors, say dimension larger
        than 50, PCA transformation is needed before the subsampling.
        """
        if not self.clustered:
            raise RuntimeError('Please first do clustering!')
        clustered_afps = self.clustered_afps
        clustered_images = self.clustered_images
        subsampled_images = OrderedDict()
        subsampled_afps = OrderedDict()
        for index, afps_per in clustered_afps.iteritems():
            indices, _ = subsampling(afps_per, method='pykdtree',
                                     cutoff_sig=cutoff_sig,
                                     rate=rate)
            subsampled_afps[index] = itemgetter(*indices)(afps_per)
            subsampled_images[index] = \
                itemgetter(*indices)(clustered_images[index])
        self.subsampled_images = subsampled_images
        self.subsampled_afps = subsampled_afps
        if save_pickle:
            with open('subsampling.pkl', 'wb') as pf:
                pickle.dump(subsampled_images, pf)
                pickle.dump(subsampled_afps, pf)
        return subsampled_images, subsampled_afps

    def affinity(self, image, method='min'):
        """Find the affinity of a given image to the as-obtained clusters
        """
        if not self.clustered:
            raise RuntimeError('Please first do clustering!')
        clustered_afps = self.clustered_afps
        metric = self.metric
        afp_target = metric.get_target_feature_vectors(image)
        if self.normalized:
            afp_target = self.scaler.transform([afp_target])[0]
        if self.pca:
            afp_target = self.pca_tranform.transform([afp_target])[0]
        confidences = []
        for index in clustered_afps.keys():
            confidence = metric.get_confidence(clustered_afps[index],
                                               afp_target,
                                               method=method)
            confidences.append(confidence)
        indices = np.argsort(confidences)
        confidences_sorted = np.sort(confidences)
        return zip(indices, confidences_sorted)


class ClusteringFailureError(Exception):
    """Error to be raised if the silhouette score of clustering is
    lower than the target score.
    """
    pass
